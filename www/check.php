<?php

/*
 * Check for current UIS session.
 *
 * This script is to service JavaScript requests to check if the
 * browser has an active session in UAS
 */

/*
 * run bootstrap and get configuration and logging
 */
define('ENV_NAME', 'app');
include '../conf/bootstrap.php';
$config = Zend_Registry::get('config');
$logger = Zend_Registry::get('ssoLog');



/*
 * Start the session
 */
Zend_Session::start();
$userData = new Zend_Session_Namespace('User_Data');


/*
 * set up authentication
 */
$auth = Zend_Auth::getInstance();
$auth->setStorage(new Zend_Auth_Storage_Session('Uis_Auth'));


/*
 * does the session id exist?
 */
$sessionId = false;
if (isset($_REQUEST['sid'])) {
	$sessionId = $_REQUEST['sid'];
}
$sessionIdentical = false;
if ($sessionId) {
	$db = Zend_Registry::get('db');
	$select = $db->select();
	$select->from('session', array('data'))
	       ->where('id = ?', $sessionId);
	$query = $db->query($select);
	$result = $query->fetch();
	
	$sessionIdentical = $result['data'];
	//echo $select;
	//var_dump($result);
}

/*
 * Check for existing UIS session
 */
if ($sessionId && $sessionIdentical) {

  $logger->log(basename(__FILE__) . ' - User has existing UIS (session, identity) = (' . $sessionId . ', false)', Zend_Log::INFO);
  echo 'var uisSessionExists = true;';

} else if (!$sessionId && $auth->hasIdentity()) {
  /*
   * Existing UIS session - go back to redirect URL with UIS session id
   */
  $logger->log(basename(__FILE__) . ' - User has existing UIS (session, identity) = (false, ' . $auth->getIdentity() . ')', Zend_Log::INFO);
   
  
  echo 'var uisSessionExists = true;';
} else {
  echo 'var uisSessionExists = false;';
}

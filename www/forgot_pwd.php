<?php

/*
 * run bootstrap and get configuration and logging
 */
define('ENV_NAME', 'app');
include '../conf/bootstrap.php';
include '../lib/Uis/ForgottenPasswordRequest.php';


// check for user input
if (!empty($_POST))
{
  // validation
  $username = '';
  $familyName = '';
  $givenName = '';
  
  $errors = array();
  if (!isset($_POST['username']) || empty($_POST['username']))
  {
    $errors['username'] = '';
  }
  else
  {
    $username = $_POST['username'];
  }
  
  if (!isset($_POST['family-name']) || empty($_POST['family-name']))
  {
    $errors['family-name'] = 'family-name';
  }
  else
  {
    $familyName = $_POST['family-name'];
  }
  
  if (!isset($_POST['given-name']) || empty($_POST['given-name']))
  {
    $errors['given-name'] = 'given-name';
  }
  else
  {
    $givenName = $_POST['given-name'];
  }

  
  
  if (empty($errors))
  {
    $personTable = new Zend_Db_Table('person');
    
    // find the person
    $select = $personTable->select()
      ->where('email = ?', $username)
      ->where('family_name = ?', $familyName)
      ->where('given_name = ?', $givenName);

   // echo $select;
   // die();
    
    if ($rows = $personTable->fetchAll($select))
    {
      if (count($rows) == 1)
      {
        $personData = $rows->current();
        if ($personData->rosetta_user_id)
        {
          // Redirect to WSK lost password page
          header('location: http://uk1.lexisnexis.com/uk/legal/WskAuth/LostPassword.aspx');
          exit(0);
        }
        
        $requestId = ForgottenPasswordRequest::create($personData->id);
        $person = ForgottenPasswordRequest::findPersonByRequestId($requestId);
        
        
		$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
		$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos($_SERVER["SERVER_PROTOCOL"], "/"))).$s;
	    $setPasswordURL = $protocol."://".$_SERVER['SERVER_NAME'].'/set_pwd.php?' . $requestId;


        
        $recipient = $personData->email;
        $subject = 'Lexis Nexis password reset';
        $from = 'From: Lexis Nexis <'.$platformConfig['emails']['from'].'>';
        $message = 'Dear '.$givenName.' '.$familyName.','. PHP_EOL .
			       ' '.PHP_EOL .
                    'A password reset has been requested on your Lexis Learning Manager account.'.PHP_EOL .
                    ' '.PHP_EOL .
                    'Please click here to reset the password:'. PHP_EOL .
                    ' '.PHP_EOL .
                    $setPasswordURL. PHP_EOL .
                    ' '.PHP_EOL .
                    'If you have not requested this reset, please call the Lexis Learning Manager helpline IMMEDIATELY on 020 7347 3575.'. PHP_EOL .
                    ' '.PHP_EOL .
                    'Kind regards,'. PHP_EOL .
                    ' '.PHP_EOL .
                    'The Lexis Learning Manager Team'. PHP_EOL .
                    ' '.PHP_EOL .
                    'LexisNexis is a trading name of REED ELSEVIER (UK) LIMITED'. PHP_EOL .
                    'Registered Office – 1-3 STRAND, LONDON WC2N 5JR'. PHP_EOL .
                    'Registered in England – Company No. 02746621'. PHP_EOL;

        //echo $message;
        //die();

        mail($recipient, $subject, $message, $from);
        header('location: forgot_pwd_sent.php');
        exit(0);

      }
    }
  }
}

include '../templates/forgot_pwd.php';

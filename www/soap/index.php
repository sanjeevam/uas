<?php

// Check if the request is from a unit test - the test suite runs in a 
// separate environment with its own database and log file

if (isset($_GET['test']))
{
  define('ENV_NAME', 'test');
}
else
{
  define('ENV_NAME', 'app');
}

// disable wsdl caching

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0'); 

// Run bootstrap and get the configuration object

include '../../conf/bootstrap.php';
$config = Zend_Registry::get('config');


// Read the WSDL and set the location

$wsdl = file_get_contents($config->soap->wsdl_file);
$location = $_SERVER['HTTP_HOST'] . '/soap/index.php';
if (isset($_GET['test']))
{
  $location .= '/?test=1';
}
$wsdl = str_replace('{LOCATION}', $location, $wsdl);


// Check if the caller is looking for the WSDL

if (isset($_GET['wsdl']))
{
  header ('content-type: text/xml');
  echo $wsdl;
  exit(0);
}


// Get the application logger

$appLog = Zend_Registry::get('appLog');


// Create the SOAP server object and hand off the request

include '../../lib/Uis/Person.php';
$server = new LexisNexis_Soap_Server(
  $appLog, 
  $config->soap->wsdl_file,
  array(
    'classmap' => array('Person' => 'Uis_Person')
  )
);

include '../../lib/Uis/Soap/Service.php';
include '../../lib/Uis/PasswordGenerator.php';
$service = new Uis_Soap_Service();
$server->setObject($service);
$server->handle();

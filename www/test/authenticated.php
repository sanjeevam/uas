<?php

/*
 * This page is WSKAuth protected, so if we get to here, the user is WSK authenticated.
 * If the user is not WSK authenticated the WSKAuth Apache module will intercept the request
 * and issue a 302 redirect to the WSKAuth login page.
 */

define('ENV_NAME', 'app');
include '../../conf/bootstrap.php';
$logger = Zend_Registry::get('ssoLog');


/*
 * Start the session
 */
Zend_Session::start();
$userData = new Zend_Session_Namespace('User_Data');


// Read the user id from the WSKAuth Cookie
$username = $_COOKIE['_$userid'];

$isAjaxRequest = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
$callType = $isAjaxRequest ? 'Ajax call' : 'Page load';
$logger->log(basename(__FILE__) . " - $callType /test/authenticated.php: user is WSK authenticated, WSK userid is $username", Zend_Log::INFO);


// check if the user is registered in UIS

$wskAuthAdapter = new LexisNexis_UserInformation_Sso_Auth_WskAdapter(Zend_Db_Table_Abstract::getDefaultAdapter());
$wskAuthAdapter
    ->setTableName('person')
    ->setIdentityColumn('username')

$wskAuthAdapter->setIdentity($username);

//$auth = Zend_Registry::get('uisAuth');
$auth = Zend_Auth::getInstance();
$auth->setStorage(new Zend_Auth_Storage_Session('Uis_Auth'));
$result = $auth->authenticate($wskAuthAdapter);

if ($result->isValid())
{
  // The user has a UIS record - set up their session
  $logger->log(basename(__FILE__) . " - WSK authenticated user has a UIS record, WSK userid is $username", Zend_Log::INFO);

  $userTable = new Zend_Db_Table('person');
  $select = $userTable->select()->where('username = ?', $result->getIdentity());
  $user = $userTable->fetchRow($select);
  
  $logger->log(basename(__FILE__) . ' - WSK authenticated user has Zend_Auth identity = ' . $result->getIdentity(), Zend_Log::INFO);
  $logger->log(basename(__FILE__) . ' - WSK authenticated user has UIS id = ' . $user->id, Zend_Log::INFO);
  
  foreach ($userData as $name => $value)
  {
    $logger->log(basename(__FILE__) . " - userData->$name = $value", Zend_Log::INFO);
  }
  
  $userData->id = $user->id;
  
  $redirectUrl = $userData->loginRedirectUrl . '?uis=' . Zend_Session::getId();
  $logger->log(basename(__FILE__) . " - Redirecting to: $redirectUrl", Zend_Log::INFO);
  header("location: $redirectUrl");
}
else
{
  echo 'REGISTRATION';
}

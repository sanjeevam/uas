<?php

/*
 * This page is WSKAuth protected, so if we get to here, the user is WSK authenticated.
 * If the user is not WSK authenticated the WSKAuth Apache module will intercept the request
 * and issue a 302 redirect to the WSKAuth login page.
 */

define('ENV_NAME', 'app');
include '../../conf/bootstrap.php';


// Read the user id from the WSKAuth Cookie
$userId = $_COOKIE['_$userid'];

$isAjaxRequest = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
$callType = $isAjaxRequest ? 'Ajax call' : 'Page load';
Zend_Registry::get('ssoLog')->log(basename(__FILE__) . " - $callType /test/index.php: user is WSK authenticated, userid is $userId", Zend_Log::INFO);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>SSO</title>
</head>
<body>
  <div id="result">1</div>
</body>
</html>

<?php

define('ENV_NAME', 'app');

include '../conf/bootstrap.php';

$config = Zend_Registry::get('config');
$logger = Zend_Registry::get('ssoLog');

$loginUrl = "https://www.lexislearningmanager.co.uk/";

//Checking the host on which UAS is running and depending on it setting the login link url. If none is match its a prod url.
if (preg_match("/staging/i", $_SERVER['HTTP_HOST'])) {
    $loginUrl = "https://learningmanager.staging.damco.lnukapps.co.uk/";
} elseif (preg_match("/uat/i", $_SERVER['HTTP_HOST'])) {
    $loginUrl = "http://learningmanager.uat.lnukapps.co.uk/";
} elseif (preg_match("/qa/i", $_SERVER['HTTP_HOST'])) {
    $loginUrl = "http://learningmanager.qa.lnukapps.co.uk/";
} elseif (preg_match("/local/i", $_SERVER['HTTP_HOST'])) {
    $loginUrl = "http://learningmanager-pe0.7.7/";
}



include '../templates/pwd_set.php';

<?php

/*
 * run bootstrap and get configuration and logging
 */
define('ENV_NAME', 'app');
include '../conf/bootstrap.php';
include '../lib/Uis/ForgottenPasswordRequest.php';
include '../lib/Uis/PasswordGenerator.php';

$requestId = reset(array_keys($_GET));

if ($person = ForgottenPasswordRequest::findPersonByRequestId($requestId))
{
  // request id is valid - send a password
  if ($password = PasswordGenerator::generate())
  {
    header('Content-type: application/json');
    echo json_encode(array('password' => $password));
  }
}

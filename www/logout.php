<?php

/*
 * Check for current UIS session - expire the session if it exists.
 * Load web page which includes Javascript which calls WskAuth logout.
 * Once the WskAuth logout page loads, redirect to the return URL
 *
 * If no UIS session - simply redirect to the return URL.
 */


/*
 * run bootstrap and get configuration and logging
 */
define('ENV_NAME', 'app');
include '../conf/bootstrap.php';
$config = Zend_Registry::get('config');
$logger = Zend_Registry::get('ssoLog');


/*
 * Start the session
 */
Zend_Session::start();
$userData = new Zend_Session_Namespace('User_Data');


$sessionId = false;
if (isset($_GET['uis'])) {
	$sessionId = $_GET['uis'];
}

/*
 * check for a return URL in the request
 */
$destination = '';
if (isset($_REQUEST['destination'])) {
	$destination = $_REQUEST['destination'];
}

$redirectUrl = false;
if (isset($_GET['ru']))
{
  if (Zend_Uri::check($_GET['ru']))
  {
    $redirectUrl = $_GET['ru'];
  }
}
if ($redirectUrl)
{
  $userData->logoutRedirectUrl = $redirectUrl;
}

if ($destination) {
	$userdata->logoutRedirectUrl .= '&destination=' . $destination;
}


/*
 * make sure we have a return URL for the user
 */
if (!$userData->logoutRedirectUrl)
{
  $message = 'No return URL';
  include '../templates/error.php';
  exit(0);
}


/*
 * set up authentication
 */
$auth = Zend_Auth::getInstance();
$auth->setStorage(new Zend_Auth_Storage_Session('Uis_Auth'));


/*
 * Check for existing UIS session
 */
if ($sessionId == Zend_Session::getId()) {

	Zend_Session::destroy();

} else if (!$sessionId && $auth->hasIdentity()) {
  /*
   * Existing UIS session - load logout page
   */
  $logger->log(basename(__FILE__) . ' - Logging out user with UIS session, identity = ' . $auth->getIdentity(), Zend_Log::INFO);
  
  /*
   * Destroy the UIS session
   */
  Zend_Session::destroy();
  
} else if ($sessionId){

	//there's a session id but not for the current user...
 
} else {
  /*
   * No existing UIS session - no action other than go to return URL.
   */
  $logger->log(basename(__FILE__) . ' - Logout called but no UIS session', Zend_Log::INFO);
}

/*
 * Redirect to return URL
 */
$redirectUrl = $userData->logoutRedirectUrl;
$logger->log(basename(__FILE__) . ' - Redirecting to: ' . $redirectUrl, Zend_Log::INFO);
header('location: ' . $redirectUrl);

function rmSession($sessionId)
{
	$db = Zend_Registry::get('db');
	$db->delete('session', 'id = \'' . $sessionId . '\'');

}

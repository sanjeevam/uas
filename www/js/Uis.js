var Uis = {};

Uis.WskAuth = function()
{
  // Private member variables
  
  var wskProtectedUrl = '/test/index.php';
  var wskAuthenticatedUrl = '/test/authenticated.php';
  var uisLoginUrl = '/uislogin.php';
  var wskAuthLoginUrl = '/wsklogin.php';
  var uisAuthenticatedUrl = '/login.php';
  var loggerUrl = '/jslogger.php';

  // Private methods
  
  /**
   * Request WskAuth locked page and check the frame content
   * If the iframe loaded the protected page the user is WSK 
   * authenticated - at this point redirect to the WSK authenticated URL
   */
  function checkWskAuthentication()
  {
    var xhr = $.ajax({
       type: 'GET',
       url: wskProtectedUrl,
       async: false
    });
    return xhr.status == 200;
  }
  
  return {
  
    // Public methods
  
    login : function ()
    {
      var uisLoginForm = $('#uisLoginForm');
      var wskLoginIFrame = $('#wskLogin');
      
      /*
       * Check if user is WSK authenticated - this will redirect if they are logged in
       */
      if (checkWskAuthentication())
      {
        document.location = wskAuthenticatedUrl;
      }
      
      /*
       * Locked page did not load: user is not WSK 
       * authenticated so we show the login form
       */
      uisLoginForm.show();
      
      /*
       * Login form submit:
       * Try UIS login first via an Ajax call.
       * If that is not successful, try WskAuth login via an iframe
       */
      uisLoginForm.submit(
        function(event)
        {
          var username = this.username.value;
          var password = this.password.value;
          
          var dataString = 'username=' + username + '&password=' + password;
          $.post(
            uisLoginUrl, 
            dataString, 
            function(data)
            {
              if (data == 1)
              {
                /*
                 * UIS login successful - user will now have valid UIS session
                 */
                document.location = uisAuthenticatedUrl;
              }
              else
              {
                /*
                 * Attach onload event to the wsk login form iframe.
                 * Once the login form submission completes, this will attempt to load
                 * a WskAuth locked page. If the page loads, the WSK login was successful,
                 * if it does not load, the login was unsuccessful and we reload the WSK
                 * login form to be ready for the next login attempt.
                 */
                wskLoginIFrame.load(
                  function(event)
                  {
                    /*
                     * After the WSK login form has been submitted and the iframe loads the response,
                     * check if the user is now WSK authenticated - this will redirect if they are logged in
                     */
                    if (checkWskAuthentication())
                    {
                      document.location = wskAuthenticatedUrl;
                    }
                    
                    /*
                     * Login has failed; unbind the onload event, to prevent infinite recursion
                     * Reload the WSK login page so it's ready for the next attempt
                     */
                    wskLoginIFrame.unbind('load');
                    wskLoginIFrame.attr('src', wskAuthLoginUrl);
                  }
                );

                /*
                 * Local login not successful - check credentials in WSK
                 * Submit the WskAuth login form in the wskLogin iframe
                 */
                wskLogin.$('#txtUserId').val(username);
                wskLogin.$('#txtPassword').val(password);
                wskLogin.$('#btnSignIn').click();
              }
            }
          );
          
          event.preventDefault();
        }
      );
    },
    
  };
}();

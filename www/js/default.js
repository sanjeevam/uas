$(function(){

	$("input:text:visible:first").focus();
	
	if(typeof $.fn.button !== 'undefined'){
		$('button.ui-button').button();
	}
	
	$('form#uisLoginForm').find('button.submit').bind('click', function(e){
	
		e.preventDefault();
		
		var $fields = $('input.not-blank'),
		$form = $('form#uisLoginForm'),
		ok = true;
		
		$.each( $fields, function(e){
			if( $(this).val().length === 0 ){
				ok = false;
				$form.addClass('form-error');
				return false;
			}
		});
		
		if(ok){
			$form.removeClass('form-error');
			$form.submit();	
		}
		
	});
	
	$('form#uisLoginForm').find('button#generatePswd').bind('click', function(e){
		e.preventDefault();
		var request_id = $('form#uisLoginForm').find('input#requestId').val();
		if( request_id.length ){
			$.ajax({
				url: '/generate_pwd.php?' + request_id,
				type: 'POST',
				dataType: 'json',
				success : function(data){
					$('#login-password').val( data.password );
					$('#login-retype').val( data.password );
				},
				failure : function(data){
				 // TODO: do this better
				 window.alert('Sorry, this item could not be removed!');
				}
			});	
		}
	});
	
});

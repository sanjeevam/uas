<?php

/*
 * Main entry page for UAS
 *
 * Check for current UIS session.
 * If no UIS session exists, load login page with form.
 */

/*
 * Run bootstrap and get configuration and logging
 */
define('ENV_NAME', 'app');

include '../conf/bootstrap.php';

$config = Zend_Registry::get('config');
$logger = Zend_Registry::get('ssoLog');

/*
 * Start the session
 */
Zend_Session::start();
$session = new Zend_Session_Namespace('User_Data');

/*
 * Check for a return URL in the request
 */
$redirectUrl = false;
if (isset($_REQUEST['ru']))
{
  if (Zend_Uri::check($_REQUEST['ru']))
  {
    $redirectUrl = $_REQUEST['ru'];
  }
}
$destination = '';
if (isset($_REQUEST['destination'])) {
	$destination = $_REQUEST['destination'];
	$session->destination = $destination;
}


if ($redirectUrl)
{
	$session->loginRedirectUrl = $redirectUrl;
}
else
{
  $message = 'No return URL';
  include '../templates/error.php';
  exit(0);
}

/*
 * Set up authentication
 */
$auth = Zend_Auth::getInstance();
$auth->setStorage(new Zend_Auth_Storage_Session('Uis_Auth'));

/*
 * Go through authentication steps
 */
$userLoggedIn = FALSE;
$loginError   = FALSE;
$userDisable  = FALSE;

if ($auth->hasIdentity())
{
  /*
   * Existing UIS session - go back to redirect URL with UIS session id
   */
  $logger->log(basename(__FILE__) . ' - User has existing UIS session, identity = ' . $auth->getIdentity(), Zend_Log::INFO);
  $userLoggedIn = true;
}
else if (!empty($_POST) || ip_user())
{

  /*
   * This is a login attempt
   */
  $username = isset($_POST['username']) ? $_POST['username'] : '';
  $password = isset($_POST['password']) ? $_POST['password'] : '';;

  /**
   * check for ip user
   */
  $ipuser = getIPuserDetails();
  if (isset($ipuser['email']))
  {
    $username = $ipuser['email'];
    $password = $ipuser['password'];
  }


  $logger->log(basename(__FILE__) . " - Attempting UIS login: username = $username", Zend_Log::INFO);

  if (!empty($username) && !empty($password))
  {
      $userData = authenticateUser($username, $password);

      /*
       * check if user is enabled
       * Commenting the disable user code on client request
       */
      if (isset($userData->enable) && $userData->enable < 1)
      {
      	$userDisable = TRUE;
      }

      /*
       * check for successful login
       */
      if (!$userDisable && $userData)
      //if ($userData)
      {
           // set up the user's session
           $session->id = $userData->id;
           $userLoggedIn = TRUE;
           $logger->log(basename(__FILE__) . ' - Login successful', Zend_Log::INFO);

      }
      else
      {
        $loginError = TRUE;
        $logger->log(basename(__FILE__) . ' - Login failed', Zend_Log::INFO);
      }

  }
}

/*
 * If the login request was successful go to the redirect page, else load the login form.
 */
if ($userLoggedIn)
{
  $redirectUrl = $session->loginRedirectUrl . '?uis=' . Zend_Session::getId();
  if ($session->destination) {
	$redirectUrl .= '&destination=' . $session->destination;
  }
  $logger->log(basename(__FILE__) . ' - Redirecting to: ' . $redirectUrl, Zend_Log::INFO);
  header('location: ' . $redirectUrl);
}
else
{
  include '../templates/login.php';
}


function getIPuserDetails()
{
  if (isset($_SERVER["REMOTE_ADDR"]) || isset($_SERVER["HTTP_X_FORWARDED_FOR"] ))
  {

    $remoteIP = $_SERVER["REMOTE_ADDR"];
    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
    {
      $remoteIP = $_SERVER["HTTP_X_FORWARDED_FOR"];
    }


    try
    {
        $db = Zend_Registry::get('db');
        $statement = $db->prepare('SELECT ip_auth.password, person.email
                                     FROM ip_auth
                                LEFT JOIN person ON person_id = person.id  
                                    WHERE ip_auth.ip_address = :ipAddress
                                      AND ip_auth.enabled    > 0 ');
        $statement->bindValue('ipAddress',    $remoteIP);
        $statement->execute();
        $person = $statement->fetchAll();
        $statement->closeCursor();
    }
    catch (Exception $e)
    {
        return array();
    }  

    if (isset($person[0]['email']) && strlen($person[0]['email']) > 1)
    {
      return array('email' => $person[0]['email'], 'password' => $person[0]['password']);
    }
    else 
    {
      return array();
    }

  }
  return array();
}

/**
 * IP User
 */
function ip_user()
{
  if (isset($_SERVER["REMOTE_ADDR"]) || isset($_SERVER["HTTP_X_FORWARDED_FOR"] ))
  {

    $remoteIP = $_SERVER["REMOTE_ADDR"];
    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
    {
      $remoteIP = $_SERVER["HTTP_X_FORWARDED_FOR"];
    }


    try
    {
        $db = Zend_Registry::get('db');
        $statement = $db->prepare('SELECT ip_auth.password, person.email
                                     FROM ip_auth
                                LEFT JOIN person ON person_id = person.id  
                                    WHERE ip_auth.ip_address = :ipAddress
                                      AND ip_auth.enabled    > 0 ');
        $statement->bindValue('ipAddress',    $remoteIP);
        $statement->execute();
        $person = $statement->fetchAll();
        $statement->closeCursor();
    }
    catch (Exception $e)
    {
        return FALSE;
    }  

    if (isset($person[0]['email']) && strlen($person[0]['email']) > 1)
    {
      return TRUE;
    }
    else 
    {
      return FALSE;
    }

  }
  return FALSE;
}



/*
 * Attempt to authenticate a user - trying UAS authentication
 * first followed by WSK authentication, if that fails
 *
 * @param $username
 * @param $password
 * @return mixed
 */
function authenticateUser($username, $password)
{
  global $auth;
  global $logger;

  $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table_Abstract::getDefaultAdapter());
  $authAdapter
    ->setTableName('person')
    ->setIdentityColumn('email')
    ->setCredentialColumn('password')
    ->setIdentity($username)
    ->setCredential(md5($password))
  ;

  $authenticationResult = false;
  $userData = false;
  $userTable = new Zend_Db_Table('person');

  try
  {
    $logger->info(basename(__FILE__) . ' - Attempting UAS authentication');
    $authenticationResult = $auth->authenticate($authAdapter);
  }
  catch (Exception $e)
  {
    $logger->log(basename(__FILE__) . ' - Authentication exception: ' . $e);
  }

  if ($authenticationResult && $authenticationResult->isValid())
  {
    try
    {
      $select = $userTable->select()->where('email = ?', $authenticationResult->getIdentity());
      $userData = $userTable->fetchRow($select);
    }
    catch (Exception $e)
    {
      $logger->log(basename(__FILE__) . ' - Exception fetching user data from database: ' . $e);
    }
  }
  else
  {
    $logger->info(basename(__FILE__) . ' - UAS authentication failed');

    /*
     * Attempt WSK authentication
     */
    $authAdapter = new Uis_WskAuthAdapter($username, $password);
    try
    {
      $logger->info(basename(__FILE__) . ' - Attempting WSK authentication');

      $authenticationResult = $auth->authenticate($authAdapter);
      if ($authenticationResult->isValid())
      {
        $logger->info(basename(__FILE__) . ' - WSK authentication successful');
        $wskUserData = $authAdapter->getWskUserData();

        // update the local database with wsk data
        $data = array(
          'family_name'     => $wskUserData->familyName,
          'given_name'      => $wskUserData->givenName,
          'email'           => $wskUserData->email
        );
        $where = $userTable->getAdapter()->quoteInto('id = ?', $matchingUser->id);
        $userTable->update($data, $where);

        // fetch the user data from the person table
        $select = $userTable->select()->where('rosetta_user_id = ?', $authenticationResult->getIdentity());
        $userData = $userTable->fetchRow($select);
      }
      else
      {
        $logger->info(basename(__FILE__) . ' - WSK authentication failed');
      }
    }
    catch (Zend_Validate_Exception $e)
    {
      $logger->info(basename(__FILE__) . ' - Authentication exception: ' . $e);
    }
    catch (Exception $e)
    {
      $logger->err(basename(__FILE__) . ' - Database exception: ' . $e);
    }
  }

  return $userData;
}

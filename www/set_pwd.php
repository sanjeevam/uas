<?php

/*
 * run bootstrap and get configuration and logging
 */
define('ENV_NAME', 'app');
include '../conf/bootstrap.php';
include '../lib/Uis/ForgottenPasswordRequest.php';

$requestId = '';

if (!empty($_GET))
{
  $requestId = reset(array_keys($_GET));
  if ($person = ForgottenPasswordRequest::findPersonByRequestId($requestId))
  {
    // show the form
  }
  else
  {
    $invalidRequestId = true;
  }
}
else if (!empty($_POST))
{
  $requestId = $_POST['request_id'];
  $password = $_POST['password'];
  $retype = $_POST['retype'];
  
  $error = false;
  if (empty($requestId))
  {
    $error = 'Missing request id';
  }
  if (empty($password) || $password != $retype)
  {
    $error = 'The passwords you typed do not match';
  }
  
  if (!$error)
  {
    if ($person = ForgottenPasswordRequest::findPersonByRequestId($requestId))
    {
      $result = ForgottenPasswordRequest::setPassword($person->id, $password);
      
      if ($result)
      {
        ForgottenPasswordRequest::remove($requestId);
        header('location: pwd_set.php');
      }
      else
      {
        $error = 'Database error';
      }
    }
    else
    {
      $error = 'Invalid request';
    }
  }
}

include '../templates/set_pwd.php';

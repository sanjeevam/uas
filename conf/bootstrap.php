<?php

header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"'); 

// dispable wsdl caching

ini_set('soap.wsdl_cache_enabled', '0');
ini_set('soap.wsdl_cache_ttl', '0');
ini_set('session.cookie_lifetime', 28800);
ini_set('session.gc_maxlifetime', 28800);

/**
 * Read config settings
 */
$platformConfigIniFile = $_SERVER['LN_APP_INIFILE'];
if (defined('ENV_NAME') and ENV_NAME =='test')
{
  $platformConfigIniFile = str_replace('.ini', '_test.ini', $platformConfigIniFile);
}
$platformConfig = parse_ini_file($platformConfigIniFile, TRUE);

/**
 * Get the library paths
 */
$zendLibDir = $platformConfig['lib']['zend_lib_dir'];
$lexisNexisLibDir = $platformConfig['lib']['lexis_nexis_lib_dir'];
$localLibDir = realpath(dirname(__FILE__) . '/../lib');

/**
 * Add Zend and LexisNexis libraries to the include path
 */
set_include_path($zendLibDir . PATH_SEPARATOR . $lexisNexisLibDir . PATH_SEPARATOR . $localLibDir . PATH_SEPARATOR . get_include_path());

/**
 * Zend Autoloader
 */
require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('LexisNexis_');
$autoloader->registerNamespace('Uis_');

/**
 * Store configuration settings
 */
$config = new Zend_Config_Ini($platformConfigIniFile);
Zend_Registry::set('config', $config);

/**
 * Logging
 */
$writer = new Zend_Log_Writer_Stream($config->log->app);
$appLog = new Zend_Log($writer);
Zend_Registry::set('appLog', $appLog);
$writer = new Zend_Log_Writer_Stream($config->log->sso);
$ssoLog = new Zend_Log($writer);
Zend_Registry::set('ssoLog', $ssoLog);


// caching

if (isset($config->memcache))
{
  $frontendOptions = array(
     'lifetime' => $config->memcache->lifetime,
     'automatic_serialization' => true
  );

  $backendOptions = array(
    'servers' => array(
      'host' => $config->memcache->host,
      'port' => $config->memcache->port,
      'persistent' =>  $config->memcache->persistent,
      'weight' => $config->memcache->weight
    )
  ) ;

  // getting a Zend_Cache_Core object
  $cache = Zend_Cache::factory(
    'Core',
    'Memcached',
    $frontendOptions,
    $backendOptions
  );
}

// database setup
$db = Zend_Db::factory('PDO_MYSQL', array(
    'host'     => $config->database->db_writer,
    'username' => $config->database->db_user,
    'password' => $config->database->db_pass,
    'dbname'   => $config->database->db_name
  )
);
$db->getProfiler()->setEnabled($config->database->enableProfiler);

Zend_Db_Table_Abstract::setDefaultAdapter($db);

// set up session storage
$config = array(
  'name'           => 'session',
  'primary'        => 'id',
  'modifiedColumn' => 'modified',
  'dataColumn'     => 'data',
  'lifetimeColumn' => 'lifetime'
);
Zend_Session::setSaveHandler(new Zend_Session_SaveHandler_DbTable($config));
Zend_Session::setOptions(array('strict' => true));


if (isset($cache))
{
  Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
}

Zend_Registry::set('db', $db);


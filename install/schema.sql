-- MySQL dump 10.11
--
-- Host: 10.21.128.101    Database: lllp_prod_uas
-- ------------------------------------------------------
-- Server version	5.0.77-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `application` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `application_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `application_admin`
--

DROP TABLE IF EXISTS `application_admin`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `application_admin` (
  `application_id` bigint(20) unsigned NOT NULL,
  `person_id` bigint(20) unsigned NOT NULL,
  `role` enum('app_admin') NOT NULL,
  PRIMARY KEY  (`person_id`,`application_id`,`role`),
  KEY `fk_application_admin_application1` (`application_id`),
  KEY `fk_application_admin_person1` (`person_id`),
  CONSTRAINT `fk_application_admin_application1` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_application_admin_person1` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `forgotten_password_request`
--

DROP TABLE IF EXISTS `forgotten_password_request`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `forgotten_password_request` (
  `id` varchar(64) NOT NULL,
  `person_id` bigint(20) unsigned NOT NULL,
  `request_time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `person` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `rosetta_user_id` varchar(64) default NULL COMMENT 'The person''s Rosetta ID. Null if authenticated locally.',
  `email` varchar(64) NOT NULL COMMENT 'The persons email address. Used for automated contact such as password recovery or for messages from contact forms.',
  `password` varchar(64) default NULL COMMENT 'Password of local users. Is null if user is authenticated by WSK.',
  `family_name` varchar(45) NOT NULL COMMENT 'The last or surname of the person.',
  `given_name` varchar(45) NOT NULL COMMENT 'The first forename of the person.',
  `middle_initials` varchar(5) default NULL COMMENT 'The person''s middle initials.',
  `title` varchar(10) default NULL COMMENT 'The person''s title, Mr, Mrs, etc.',
  `rosetta_lbu_account_id` varchar(64) default NULL,
  `last_updated_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `person_email` (`email`),
  UNIQUE KEY `person_rosetta_user_id` (`rosetta_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Holds details of a person who is known to the system.';
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `session` (
  `id` varchar(32) NOT NULL,
  `modified` int(11) default NULL,
  `lifetime` int(11) default NULL,
  `data` text,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;


--- 
--- IP Authentication table
---
DROP TABLE IF EXISTS `ip_auth`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ip_auth` (
  `id`         bigint(20)   unsigned NOT NULL auto_increment,
  `person_id`  int(11)      default NULL,
  `password`   varchar(255) default '',
  `ip_address` varchar(255) default NULL,
  `enabled`    tinyint(1)   default 1,
  PRIMARY KEY  (`id`),
  KEY `person_id`  (`person_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;



--
-- Dumping routines for database 'lllp_prod_uas'
--
DELIMITER ;;
DELIMITER ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-01-05 13:44:57

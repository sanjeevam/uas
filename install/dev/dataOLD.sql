INSERT INTO `application` (`id`, `name`, `username`, `password`) VALUES
  (1, 'Dispute Resolution', 'elearning', 'b607148f7a90a019f177aed30507f624'),
  (2, 'Webinars', 'webinars', '888f0a0a5eea3bb8835d96be14a2951a'),
  (3, 'Legal Learning Platform', 'legallearning', 'f1800ea338a2d310c68eed44aaad2dc5');


INSERT INTO `person` (`id`, `rosetta_user_id`, `email`, `password`, `family_name`, `given_name`, `middle_initials`, `title`, `rosetta_lbu_account_id`, `last_updated_date`) VALUES
(2,  NULL, 'lexisadmin@test.com',       'f3d39c5f1061d660ec777cf327d9d7af', 'Admin', 'Lexis',        'J', 'Mr', NULL, '2010-07-17 19:59:10');

INSERT INTO `application_admin` VALUES 
(1, 2, 'app_admin'),
(2, 2, 'app_admin'),
(3, 2, 'app_admin');




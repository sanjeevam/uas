
-- This script migrates user data from a LexisWebinars database to a UAS database.

-- Pre-conditions
--   * The UAS database has been created using the schema.sql script.
--   * The script uas_import_static_data.sql has been run.
--   * A session variable @cids has been defined containing either the string 'ALL' (to import all 
--     users for all clients) or a single LexisWebinars cid value (to import users of a specific client).

-- Post-conditions
--   * The specified users have been imported.




SELECT 'Copying the employees of the client(s).' AS ' ';

-- Copy all the client user, those whose ll_user.cid matches the client being migrated.

-- Note that administrative users have already been imported by the uas_import_static_data.sql 
-- therefore we use INSERT IGNORE to prevent errors if script attempts to insert them again. 

INSERT IGNORE INTO person 
  (id, email, password, family_name, given_name, middle_initials, title)
SELECT 
  u.uid,
  u.mail,
  u.pass,
  p.last_name,
  p.first_name,
  p.middle_initials,
  p.title
FROM 
  source.ll_user AS p,
  source.users AS u
WHERE
  (@cids = 'ALL' OR p.cid = @cids) AND
  p.uid = u.uid;

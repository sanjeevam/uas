
-- This script does an initial population of a UAS database with static data and
-- administrative users from a LexisWebinars database.

-- Pre-conditions
--   * An empty UAS database has been created using the schema.sql script.
--   * The the LexisWebinars database that is to be used as the source of administrative user data
--     is called 'source'.
--   * The static data for the application table in this script is correct and complete.

-- Post-conditions
--   * The static data table application is populated.
--   * Person rows have been created from all users with a role of administrator in the source
--     LexisWebinars database.
--   * All persons are administrators of all applications. 





SELECT 'Inserting application rows.' AS '';

-- Insert application rows. This is hard coded and currently we have two channels and the learning 
-- platform itself.

-- INSERT INTO application 
--   (id, name, username, password) 
-- VALUES
--   (1, 'Dispute Resolution', 'dispute', MD5('dispute')),
--   (2, 'Webinars', 'webinars', MD5('webinars')),
--   (3, 'Legal Learning Platform', 'legallearning', MD5('legallearning'));


  
  
SELECT 'Creating person rows for administrators.' AS '';

-- Here we are creating person records for administrators. We ignore the uid=0 (anonymous user) and uid=1 
-- (superuser) rows. Additional data obtained from source.ll_user table.

INSERT INTO person 
  (id, email, password, family_name, given_name, middle_initials, title)
SELECT 
  u.uid,
  u.mail,
  u.pass,
  p.last_name,
  p.first_name,
  p.middle_initials,
  p.title
FROM 
  source.users AS u
  LEFT JOIN source.ll_user AS p ON (u.uid = p.uid)
  INNER JOIN source.users_roles AS ur ON (u.uid = ur.uid)
  INNER JOIN source.role AS r ON (ur.rid = r.rid)
WHERE
  u.uid > 2 AND
  r.name = 'administrator'
ORDER BY 
  u.uid;
  
  
  
  
SELECT 'Setting up administrators.' AS '';

-- User having a role of 'administrator' become administrators of all
-- applications.

INSERT IGNORE INTO application_admin 
  (application_id, person_id, role) 
SELECT
  a.id,
  p.id,
  'app_admin'
FROM
  person AS p,
  application AS a
ORDER BY
  a.id;

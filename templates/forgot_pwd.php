<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Forgotten Password</title>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/main.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/css/ui/jquery-ui-1.8.1.custom.css" />
    <script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
		<script type="text/javascript" src="/js/ui/jquery-ui-1.8.1.custom.js"></script>
		<script type="text/javascript" src="/js/default.js"></script>
    <!--[if lte IE 6]>
    	<link rel="stylesheet" type="text/css" href="/css/ie6.css" media="all" />
   	<![endif]-->
  

<!-- Start -- code for cookie banner on top of site -- Start -->

<script type="text/javascript">
		(function() {
		  var euCookiesLaw = document.createElement('script'); euCookiesLaw.type = 'text/javascript'; euCookiesLaw.async = true;
		  euCookiesLaw.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'lexisuk.co.uk/eucookie-without-jquery/eucookies.plugin.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(euCookiesLaw, s);
		})();
</script>

<!-- End -- code for cookie banner on top of site -- End -->    
    
  </head>
<body>
<div id="header">
  <div class="wrap">
      <h1>Lexis<sup>&reg;</sup>Learning Manager</h1>
      <p class="home hide"><a href="/" title="Home">Go back to homepage</a></p>
  </div>
</div>
<div id="page">
  <div id="box-outer">
  	<div id="box">
      <form id="uisLoginForm" action="/forgot_pwd.php" method="post" <?php if (isset($requestError) && $requestError) echo 'class="form-error"'; ?>>
        <h2 class="icon secure">Forgotten Password</h2>
        <div class="ui-state-highlight ui-corner-all">
       		<p><span class="ui-icon ui-icon-info"></span><span class="message">Please complete the information below.</span></p>
        </div>
        <div class="ui-state-error ui-corner-all">
					<?php
					if ($requestError)
          {
            $message = 'Cannot find match, please try again.';
          }
          else
          {
            $message = 'Please complete all fields.';
          }
					?>
        	<p><span class="ui-icon ui-icon-alert"></span><span class="message"><?php echo $message; ?></span></p>
        </div>
        <dl class="login">
          <dt><label for="login-username">Your email:</label></dt>  
          <dd><input tabindex="1" class="text not-blank" type="text" name="username" id="login-username" /></dd>
          
          <dt><label for="login-family-name">Family name:</label></dt>
          <dd><input class="text not-blank" type="text" name="family-name" id="login-family-name" /></dd>
          
          <dt><label for="login-given-name">Given name:</label></dt>
          <dd><input class="text not-blank" type="text" name="given-name" id="login-given-name" /></dd>
          
          <dt>&nbsp;</dt>
          <dd class="button">
            <button class="ui-button ui-button-text-icon submit" type="submit">
              <span class="ui-button-icon-primary ui-icon ui-icon-key"></span>
              <span class="ui-button-text">Request Password</span>
            </button>
          </dd>
        </dl>
        <div class="clear"></div>
      </form>
    </div>
  	<div id="box-bottom"></div>
  </div>
  <div id="footer">
        <p>Copyright &copy; <?php echo date('Y', time()); ?>. All rights reserved. | <a target="_blank" href="http://www.lexisnexis.co.uk/privacy/">Privacy Policy</a></p>
  </div>
</div>
</body>
</html>
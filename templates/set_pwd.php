<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Forgotten Password</title>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/main.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/css/ui/jquery-ui-1.8.1.custom.css" />
    <script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
		<script type="text/javascript" src="/js/ui/jquery-ui-1.8.1.custom.js"></script>
		<script type="text/javascript" src="/js/default.js"></script>
    <!--[if lte IE 6]>
    	<link rel="stylesheet" type="text/css" href="/css/ie6.css" media="all" />
   	<![endif]-->
    
<!-- Start -- code for cookie banner on top of site -- Start -->

<script type="text/javascript">
		(function() {
		  var euCookiesLaw = document.createElement('script'); euCookiesLaw.type = 'text/javascript'; euCookiesLaw.async = true;
		  euCookiesLaw.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'lexisuk.co.uk/eucookie-without-jquery/eucookies.plugin.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(euCookiesLaw, s);
		})();
</script>

<!-- End -- code for cookie banner on top of site -- End -->   
    
  </head>
<body>
<div id="header">
  <div class="wrap">
           <h1>Lexis<sup>&reg;</sup>Learning Manager</h1>
      <p class="home hide"><a href="/" title="Home">Go back to homepage</a></p>
  </div>
</div>
<div id="page">
  <div id="box-outer">
  	<div id="box">
      <form id="uisLoginForm" action="/set_pwd.php" method="post" <?php if ($error) echo 'class="form-error"'; ?>>
        <h2 class="icon secure">Create New Password</h2>
        <div class="ui-state-highlight ui-corner-all">
       		<p><span class="ui-icon ui-icon-info"></span><span class="message">Please enter and confirm your new password. Alternatively, select 'Generate' for the system to suggest a password for you.</span></p>
        </div>
        <div class="ui-state-error ui-corner-all">
					<?php
					if ($error)
          {
            $message = $error;
          }
          else
          {
            $message = 'Please complete all fields.';
          }
					?>
        	<p><span class="ui-icon ui-icon-alert"></span><span class="message"><?php echo $message; ?></span></p>
        </div>
        <dl class="password">
          <dt><label for="login-password">New password:</label></dt>  
          <dd>
          	<input tabindex="1" class="text not-blank" type="text" name="password" id="login-password" />
          	<button id="generatePswd" class="ui-button ui-button-text-icon font-reset" type="submit">
              <span class="ui-button-icon-primary ui-icon ui-icon-gear"></span>
              <span class="ui-button-text">Generate</span>
            </button>
          </dd>
          <dt><label for="login-retype">Confirm password:</label></dt>
          <dd><input class="text not-blank" type="text" name="retype" id="login-retype" /></dd>
          <dt>&nbsp;</dt>
          <dd class="button">
            <button class="ui-button ui-button-text-icon submit" type="submit">
              <span class="ui-button-icon-primary ui-icon ui-icon-key"></span>
              <span class="ui-button-text">Update Password</span>
            </button>
          </dd>
        </dl>
        <div class="clear"></div>
        <input type="hidden" name="request_id" id="requestId" value="<?php echo $requestId ?>" />
      </form>
    </div>
  	<div id="box-bottom"></div>
  </div>
  <div id="footer">
    <p>Copyright &copy; 2010. All rights reserved. | <a target="_blank" href="http://www.lexisnexis.co.uk/privacy/">Privacy Policy</a></p>
  </div>
</div>
</body>
</html>
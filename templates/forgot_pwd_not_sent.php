<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Forgotten Password</title>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/main.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/css/ui/jquery-ui-1.8.1.custom.css" />
    <script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
		<script type="text/javascript" src="/js/ui/jquery-ui-1.8.1.custom.js"></script>
		<script type="text/javascript" src="/js/default.js"></script>
    <!--[if lte IE 6]>
    	<link rel="stylesheet" type="text/css" href="/css/ie6.css" media="all" />
   	<![endif]-->
    
<!-- Start -- code for cookie banner on top of site -- Start -->

<script type="text/javascript">
		(function() {
		  var euCookiesLaw = document.createElement('script'); euCookiesLaw.type = 'text/javascript'; euCookiesLaw.async = true;
		  euCookiesLaw.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'lexisuk.co.uk/eucookie-without-jquery/eucookies.plugin.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(euCookiesLaw, s);
		})();
</script>

<!-- End -- code for cookie banner on top of site -- End -->   
    
  </head>
<body>
<div id="header">
  <div class="wrap">
      <h1>Lexis<sup>&reg;</sup>Learning Manager</h1>
      <p class="home hide"><a href="/" title="Home">Go back to homepage</a></p>
  </div>
</div>
<div id="page">
  <div id="box-outer">
  	<div id="box">
      <form id="uisLoginForm" action="/forgot_pwd.php" method="post" <?php if ($requestError) echo 'class="form-error"'; ?>>
        <h2 class="icon secure">Forgotten password</h2>
        <div class="ui-state-error ui-corner-all" style="display:block;">
            <p><span class="ui-icon ui-icon-alert"></span><span class="message">Sorry, the email to reset your password could not get sent.<br />Please call the Lexis Learning Manager helpline on 020 7347 3575</span></p>
        </div>
        <div class="clear"></div>
      </form>
    </div>
  	<div id="box-bottom"></div>
  </div>
  <div id="footer">
    <p>Copyright &copy; <?php echo date('Y', time()); ?>. All rights reserved. | <a target="_blank" href="http://www.lexisnexis.co.uk/privacy/">Privacy Policy</a></p>
  </div>
</div>
</body>
</html>
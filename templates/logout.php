<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>UIS logout</title>
    <?php if ($useWskAuth): ?>
    <script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="/js/Uis.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
      Uis.WskAuth.logout('<?php echo $wskLogoutUrl; ?>', '<?php echo $logoutRedirectUrl; ?>');
    });
    </script>
    <?php endif; ?>
    <style type="text/css">
      #loginForm { display:none; }
    </style>
  </head>
  <body>
    <p>Logout</p>
    
    <p><?php echo $wskLogoutUrl; ?></p>
    
    <?php if ($useWskAuth): ?>
    <div id="wskLoginContainer">
      <iframe name="wskLogout" id="wskLogout" src="" width="100%" height="300px"></iframe>
    </div>
    <?php endif; ?>
    
  </body>
</html>
<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema" 
        targetNamespace="http://getdocumentswithcustomview.retrieve.services.v1.wsapi.lexisnexis.com" 
        xmlns:tns="http://getdocumentswithcustomview.retrieve.services.v1.wsapi.lexisnexis.com" 
        xmlns:security="http://security.common.services.v1.wsapi.lexisnexis.com"
		xmlns:result="http://result.common.services.v1.wsapi.lexisnexis.com"
        elementFormDefault="qualified" 
        attributeFormDefault="unqualified" 
        version="1.0">

	<import namespace="http://security.common.services.v1.wsapi.lexisnexis.com"
        	schemaLocation="Security.xsd" />

	<import namespace="http://result.common.services.v1.wsapi.lexisnexis.com"
			schemaLocation="Result.xsd" />

	<import namespace="http://common.search.services.v1.wsapi.lexisnexis.com"
			schemaLocation="SearchCommon.xsd" />

	<!-- Message elements -->
	<element name="GetDocumentsWithCustomView" type="tns:GetDocumentsWithCustomView">
		<annotation>
			<documentation>
				Allows a client to retrieve documents in a custom view.
			</documentation>
		</annotation>
	</element>

	<element name="GetDocumentsWithCustomViewResponse" type="result:RetrievalResponse">
		<annotation>
			<documentation>
				Returns the documents requested by the client.
			</documentation>
		</annotation>
	</element>

	<!-- Types -->
	<complexType name="GetDocumentsWithCustomView">
		<sequence>
			<element name="binarySecurityToken" type="security:BinarySecurityToken">
				<annotation>
					<documentation>
						Security token that must be provided on all authenticated requests.
					</documentation>
				</annotation>
			</element>
			<element name="searchId" type="string">
				<annotation>
					<documentation>
						Identifier that refers to all pertinent information related to the original search.
					</documentation>
				</annotation>
			</element>
			<element name="retrievalOptions" type="tns:CustomViewRetrievalOptions">
				<annotation>
					<documentation>
						This structure contains details about how retrieved
						documents are to be presented.  Also includes the range of documents
						to return.  If absent, no documents will be returned.
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="CustomViewRetrievalOptions">
		<sequence>
			<element name="documentMarkup" type="result:DocumentMarkup" minOccurs="0">
				<annotation>
					<documentation>Defines the particular markup in which documents are to be retrieved.</documentation>
				</annotation>
			</element>
			<element name="segmentNameList" type="tns:SegmentNameList">
				<annotation>
					<documentation>Defines the segments to be included in the custom view.</documentation>
				</annotation>
			</element>
			<element name="documentRange" type="result:Range">
				<annotation>
					<documentation>Defines the range of documents to be retrieved.</documentation>
				</annotation>
			</element>
			<element name="includeExtendedOutput" type="boolean" minOccurs="0">
				<annotation>
					<documentation>
						<example>true</example>
					</documentation>
				</annotation>
			</element>
			<element name="includeShepardsSignals" type="boolean" minOccurs="0">
				<annotation>
					<documentation>Allows the retrieval of Shepard's Signals.</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>


	<complexType name="SegmentNameList">
		<sequence>
			<element name="segmentName" type="string" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>
						An identifier associated with a single LexisNexis document segment.
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

</schema>

<!--
                      LexisNexis CONFIDENTIAL

  This document is the property of LexisNexis, and its contents are
  proprietary to LexisNexis. Reproduction in any form is prohibited.
  Finders are asked to return this document to:
  LexisNexis, P.O. Box 933, Dayton, Ohio  45401

  Copyright (c) 2007 by LexisNexis
  All Rights Reserved
-->

<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema" 
        targetNamespace="http://browsesources.source.services.v1.wsapi.lexisnexis.com" 
        xmlns:tns="http://browsesources.source.services.v1.wsapi.lexisnexis.com" 
        xmlns:common="http://common.services.v1.wsapi.lexisnexis.com"
        xmlns:security="http://security.common.services.v1.wsapi.lexisnexis.com" 
        xmlns:sourcecommon="http://common.source.services.v1.wsapi.lexisnexis.com" 
        elementFormDefault="qualified" 
        attributeFormDefault="unqualified" 
        version="1.0">

  <import namespace="http://common.services.v1.wsapi.lexisnexis.com"
          schemaLocation="Common.xsd"/>

   <import namespace="http://security.common.services.v1.wsapi.lexisnexis.com"
           schemaLocation="Security.xsd" />           
           
   <import namespace="http://common.source.services.v1.wsapi.lexisnexis.com"
           schemaLocation="SourceCommon.xsd" />           

   <!-- Define request/response elements -->
   <element name="BrowseSources" type="tns:BrowseSources">
      <annotation>
         <documentation>
            Allows you to browse the source directory hierarchy associated
            with your credentials (as dictated by your menu). The source 
            directory is organized as a tree-like structure containing folders 
            and sources. Since the names of folders and sources can change, 
            folder identifiers and source identifiers are used to maintain 
            persistence. 
         </documentation>
      </annotation>
   </element>
   <element name="BrowseSourcesResponse" type="tns:BrowseSourcesResponse">
      <annotation>
         <documentation>
            Includes information about the requested node in the source 
            hierarchy (i.e. tree).
         </documentation>
      </annotation>
   </element>
         
   <!-- Define request types -->
   <complexType name="BrowseSources">
      <sequence>
         <element name="locale" type="common:Locale">
         <annotation>
				<documentation>
					Optional locale indicating language of request.
				</documentation>
			</annotation>
		 </element>
         <element name="binarySecurityToken" type="security:BinarySecurityToken">
            <annotation>
               <documentation>
                  <example>f4348ad8876542bc93748c89fb98a7e</example>
                  Security token that must be provided on all non-authentication
                  requests.
               </documentation>
            </annotation>
         </element>
         <element name="folderId" type="integer" minOccurs="0">
            <annotation>
               <documentation>
                  The node indicating the location in the tree the client is currently browsing.
               </documentation>
            </annotation>
         </element>
         <element name="sourcePackageId" type="string" minOccurs="0">
            <annotation>
               <documentation>
                  The menu you want to browse with.
				  Default: all menus.
               </documentation>
            </annotation>
         </element>
          <element name="useCSP" type="boolean" minOccurs="0">
            <annotation>
              <documentation>
                Internal Use Only!
                Use Combined Search Pricing Rules/Logic.
              </documentation>
            </annotation>
          </element>
      </sequence>
   </complexType>
   <!-- Define response types -->
   <complexType name="Folder">
      <sequence>
         <element name="name" type="string">
            <annotation>
               <documentation>
                  The name of a Category node.
               </documentation>
            </annotation>
         </element>
         <element name="folderId" type="integer">
            <annotation>
               <documentation>
                  An identifier that is associated with a particular Category Folder in 
                  the source directory.
               </documentation>
            </annotation>
         </element>
      </sequence>
   </complexType>
   
   <complexType name="FolderList">
      <sequence>
         <element name="folder" type="tns:Folder" minOccurs="0" maxOccurs="unbounded">
            <annotation>
               <documentation>
                  Specific information about a Node (name and ID).
               </documentation>
            </annotation>
         </element>
      </sequence>
   </complexType>

   <complexType name="BrowseSourcesResponse">
      <sequence>
         <element name="sourceList" type="sourcecommon:SourceList">
            <annotation>
               <documentation>
                  A list of sources contained within the requested Category Node.
               </documentation>
            </annotation>
         </element>
         <element name="folderList" type="tns:FolderList">
         	<annotation>
         	   <documentation>
         	      A list of sub-Category Nodes contained within the requested Category.
         	   </documentation>
         	</annotation>
         </element>       
      </sequence>
   </complexType>   
</schema>

<!--
                      LexisNexis CONFIDENTIAL

  This document is the property of LexisNexis, and its contents are
  proprietary to LexisNexis. Reproduction in any form is prohibited.
  Finders are asked to return this document to:
  LexisNexis, P.O. Box 933, Dayton, Ohio  45401

  Copyright (c) 2004 by LexisNexis
  All Rights Reserved
-->

<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema"
        targetNamespace="http://browsesupportedregions.dossier.services.v1.wsapi.lexisnexis.com"
        xmlns:tns="http://browsesupportedregions.dossier.services.v1.wsapi.lexisnexis.com"
        xmlns:common="http://common.services.v1.wsapi.lexisnexis.com"
        xmlns:security="http://security.common.services.v1.wsapi.lexisnexis.com"
        elementFormDefault="qualified"
        attributeFormDefault="unqualified"
        version="1.0">

	<import namespace="http://security.common.services.v1.wsapi.lexisnexis.com" 
			schemaLocation="../Security.xsd"/>

	<import namespace="http://common.services.v1.wsapi.lexisnexis.com" 
			schemaLocation="../Common.xsd"/>

	<!-- Define request/response elements -->
	<element name="BrowseSupportedRegions" type="tns:BrowseSupportedRegions">
		<annotation>
			<documentation>Retrieve a list of regions to restrict a Dossier Search to.</documentation>
		</annotation>
	</element>
	<element name="BrowseSupportedRegionsResponse" type="tns:BrowseSupportedRegionsResponse">
		<annotation>
			<documentation>A list of regions at the requested level.</documentation>
		</annotation>
	</element>

	<!-- Define request/response types -->
	<complexType name="BrowseSupportedRegions">
		<sequence>
			<element name="binarySecurityToken" type="security:BinarySecurityToken">
				<annotation>
					<documentation>Security token that must be provided on all non-authentication requests.</documentation>
				</annotation>
			</element>
			<element name="regionId" type="string" minOccurs="0">
				<annotation>
					<documentation>An identifier used to represent a specific region in
                  the region list.  If no region is supplied, a list of the
				  broadest regions (a country list) will be returned.</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="BrowseSupportedRegionsResponse">
		<sequence>
			<element name="regionList" type="tns:RegionList">
				<annotation>
					<documentation>A list of regions.</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="RegionList">
		<sequence>
			<element name="Region" type="tns:Region" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>A region.</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="Region">
		<sequence>
			<element name="regionName" type="string">
				<annotation>
					<documentation>The region's name.</documentation>
				</annotation>
			</element>
			<element name="regionId" type="string">
				<annotation>
					<documentation>The region's id, which can be used in a
					Dossier Search or used to call the BrowseSupportedRegions operation
					again to get sub-regions (if subregionsAvailable = true).</documentation>
				</annotation>
			</element>
			<element name="regionType" type="string">
				<annotation>
					<documentation>Various region types (e.g. "country", "state", etc.) are 
					supported by Dossier and can be used when searching the Dossier service. 
					For example, you could locate a particular country by including the
					"country(12345)" query in your Dossier search.</documentation>
				</annotation>
			</element>
			<element name="subregionsAvailable" type="boolean">
				<annotation>
					<documentation>A flag that indicates that subregions are available,
					and that BrowseSupportedRegions may be called again using this regionId to
					obtain the list of subregions.</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>
</schema>
<!--
                      LexisNexis CONFIDENTIAL

  This document is the property of LexisNexis, and its contents are
  proprietary to LexisNexis. Reproduction in any form is prohibited.
  Finders are asked to return this document to:
  LexisNexis, P.O. Box 933, Dayton, Ohio  45401

  Copyright (c) 2005 by LexisNexis
  All Rights Reserved
-->
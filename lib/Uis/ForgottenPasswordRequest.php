<?php

class ForgottenPasswordRequest
{
  /**
   * Finder function: get by id
   *
   * @param int $id
   * @return Zend_Db_Table_Row
   */
  public static function findPersonByRequestId($id)
  {
    #TODO: exception handling
    $requestTable = new Zend_Db_Table('forgotten_password_request');
    $select = $requestTable->select()
      ->where('id = ?', $id);
    
    if ($request = $requestTable->fetchRow($select))
    {
      $personTable = new Zend_Db_Table('person');
      $select = $personTable->select()
        ->where('id = ?', $request->person_id);
        
      if ($person = $personTable->fetchRow($select))
      {
        return $person;
      }
    }
    
    return false;
  }

  /**
   * Create a request for a person id
   *
   * @param int $personId
   * @return string The request id
   */
  public static function create($personId)
  {
    #TODO: exception handling
    $requestId = uniqid($personId);
    
    $requestData = array(
      'id' => $requestId,
      'person_id' => $personId
    );
    
    $requestTable = new Zend_Db_Table('forgotten_password_request');
    $requestTable->insert($requestData);
    /*
    $select = $requestTable->select()
      ->where('id = ?', $requestId);
      
    $row = $requestTable->fetchRow($select);
    print_r($row->toArray());  
    */
    return $requestId;
  }
  
  /**
   * Reset a person's password
   * 
   * @param int $personId
   * @param string $password
   * @return boolean Success
   */
  public static function setPassword($personId, $password)
  {
    #TODO: exception handling
    $personTable = new Zend_Db_Table('person');
    
    $data = array(
      'password' => md5($password)
    );
    
    $where = $personTable->getAdapter()->quoteInto('id = ?', $personId);
    
    return $personTable->update($data, $where);
  }
  
  /**
   * Remove a request, optionally remove expired requests at the same time
   *
   * @param int $requestId
   * @param boolean $removeExpired
   * @return boolean Success
   */
  public static function remove($requestId, $removeExpired = false)
  {
    #TODO: exception handling
    $requestTable = new Zend_Db_Table('forgotten_password_request');
    
    $date = new DateTime();
    $date->modify('-1 hour');
        
    $where = $requestTable->getAdapter()->quoteInto('id = ?', $requestId);
    $requestTable->delete($where);
    
    if ($removeExpired)
    {
      $where = $requestTable->getAdapter()->quoteInto('request_time < ?', $date->format('Y-m-d H:i:s'));
      $requestTable->delete($where);
    }
    
    return true;
  }
}

<?php

/**
 * Implementation of Zend_Auth_Adapter_Interface for WSK authentication
 *
 * @package Uis
 */

class Uis_WskAuthAdapter implements Zend_Auth_Adapter_Interface
{
  /**
   * Username
   *
   * @var string
   */
  private $_username;
  
  /**
   * Password
   *
   * @var string
   */
  private $_password;
  
  /**
   * Storage for user data returned by WSK
   *
   * @var stdClass
   */
  private $_wskUserData;
  
  /**
   * Public constructor
   *
   * @param string $username
   * @param string $password
   * @return void
   */
  public function __construct($username, $password)
  {
    $this->_username = $username;
    $this->_password = $password;
  }
  
  /**
   * Implementation of Zend_Auth_Adapter_Interface::authenticate()
   *
   * @return Zend_Auth_Result
   * @throws Zend_Validate_Exception
   */
  public function authenticate()
  {
    $userTable = new Zend_Db_Table('person');
    
    $select = $userTable->select()->where('rosetta_user_id = ?', $this->_username);
    if ($matchingUser = $userTable->fetchRow($select))
    {
      try
      {
        $wskClient = new Uis_Soap_WskClient();
        #TODO: investigate WSK - use getUserInfo or authenticateEndUser
        $this->_wskUserData = $wskClient->getUserInfo($this->_username, $this->_password);
      }
      catch (Exception $e)
      {
        throw new Zend_Validate_Exception('Error calling Uis_Soap_WskClient::getUserInfo');
      }
      
      return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $this->_username);
    }
    
    return new Zend_Auth_Result(Zend_Auth_Result::FAILURE, $this->_username);
  }
  
  /**
   * Get the user data returned by WSK during authentication
   *
   * @return stdClass
   */
  public function getWskUserData()
  {
    return $this->_wskUserData;
  }
}

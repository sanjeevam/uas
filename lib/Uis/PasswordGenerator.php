<?php

class PasswordGenerator
{
  /**
   * Generate a password by concatenating words 
   * from a dictionary file with a random number
   *
   * @return string
   */
  public static function generate()
  {
    $dictionaryFile = dirname(__FILE__) . '/../passwords.php';
    
    if (is_readable($dictionaryFile))
    {
      $passwords = file($dictionaryFile);
    }
  
    $random = rand(0, count($passwords)-1);
    $word = trim($passwords[$random]);
    
    if (!empty($word))
    {
      $number = rand(100, 999);
      return $word.$number;
    }
    
    return false;
  }
}

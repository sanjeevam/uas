<?php

class Uis_Person
{
  /**
   * Id
   *
   * @var int
   */
  public $id;

  /**
   * Id
   *
   * @var string
   */
  public $rosettaUserId;
  
  /**
   * Id
   *
   * @var string
   */
  public $email;
  
  /**
   * Id
   *
   * @var string
   */
  public $password;
  
  /**
   * Id
   *
   * @var string
   */
  public $familyName;
  
  /**
   * Id
   *
   * @var string
   */
  public $givenName;
  
  /**
   * Id
   *
   * @var string
   */
  public $middleInitials;
  
  /**
   * Id
   *
   * @var string
   */
  public $title;
  
  /**
   * Id
   *
   * @var string
   */
  public $rosettaLbuAccountId;

  /**
   * Array of field validators
   *
   * @var array
   */
  private $_validators = array();

  /**
   * Default constructor
   *
   * @return void
   */
  public function __construct()
  {
    $this->_validators = array(
      'familyName' => array(
        'allowEmpty' => false
      ),
      'givenName' => array(
        'allowEmpty' => false
      ),
      'email' => array(
        'EmailAddress',
        'allowEmpty' => false
      )
    );
  }
  
  /**
   * Check member variables to ensure that required parameters have 
   * been set and have correct data types
   *
   * @return boolean
   */
  public function validate()
  {
    $reflect = new ReflectionObject($this);
    $properties = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
    
    $data = array();
    foreach ($properties as $property)
    {
      $propertyName = $property->getName();
      $data[$propertyName] = $this->$propertyName;
    }
    
    $input = new Zend_Filter_Input(null, $this->_validators, $data, null);
    $isValid = $input->isValid();

    if ($this->id)
    {
      // this is an edit: rossetta ids cannot be edited
      if ($this->rosettaUserId)
      {
        $isValid = false;
      }
    }
    else
    {
      // this is a new person: must have a password or a rosetta id
      if (!($this->password XOR $this->rosettaUserId))
      {
        $isValid = false;
      }
    }
    
    return $isValid;
  }
}

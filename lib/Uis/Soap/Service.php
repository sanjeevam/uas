<?php

/**
 * API methods of the UIS SOAP server
 *
 * Tests for this service are in https://svn.lnukapps.co.uk/svn/lnukapps/clientsolutions/learningservicetests/trunk
 * The test suite requires a separate test database for its own data fixtures.
 *
 * @package Uis_Soap
 */

class Uis_Soap_Service
{
  /**
   * Authetication flag: this is set by the UsernameToken method if the SOAP request
   * contains a UsernameToken header. This must be checked by all service methods.
   *
   * @var boolean
   */
  private $_authenticated = false;
  
  /**
   * The username of the caller: set in the UsernameToken SOAP header.
   *
   * @var string
   */
  private $_username = '';
  
  /**
   * ID of the calling application
   *
   * @var string
   */
  private $_applicationId;
  
  /**
   * Logger
   *
   * @var Zend_Log
   */
  private $_logger = null;
  
  /**
   * Constructor
   *
   * @return void
   */
  public function __construct()
  {
    $this->_logger = Zend_Registry::get('appLog');
  }
   
  /**
   * Authentication method: implementation of Ws-security standard auth.
   * If a malicious user attempts to call the service without a UsernameToken header,
   * this method will not be called and the authentication flag will be false.
   *
   * @param string $username
   * @param string $password
   * @return void
   * @throws LexisNexis_Soap_AuthException
   */
  public function UsernameToken($username, $password)
  {
    $this->_logger->log(__METHOD__ . ": username = $username, password = $password", Zend_Log::INFO);
    
    $applicationTable = new Zend_Db_Table('application');
    $select = $applicationTable->select()->where('username = ?', $username);
    $applicationData = $applicationTable->fetchRow($select);
    
    if($applicationData && $applicationData->password == md5($password))
    {
      $this->_authenticated = true;
      $this->_username = $username;
      $this->_applicationId  = $applicationData->id;
    }
    else
    {
      throw new LexisNexis_Soap_AuthException("Authentication credentials incorrect: username = $username, password = $password");
    }
  }
  
  /**
   * Get the details of a person associated with a given session id
   *
   * @param string $sessionId
   * @return array 
   * <ul>
   * <li>Person object</li>
   * <li>Array of admin and roles</li>
   * </ul>
   * @throws LexisNexis_Soap_SessionException, Zend_Db_Exception
   */
  public function getPersonBySessionId($sessionId)
  {
    $this->_authenticate();
    $this->_logger->log(__METHOD__ . ' - sessionId = ' . $sessionId, Zend_Log::INFO);
    
    // Start a session for the given id
    Zend_Session::setId($sessionId);
    Zend_Session::start();
    $userData = new Zend_Session_Namespace('User_Data');
    
    $person = null;
    
    if ($userData->__isset('id'))
    {
      $this->_logger->log(__METHOD__ . ' - session data contains person id = ' . $userData->id, Zend_Log::INFO);
      $person = $this->_getPersonById($userData->id);
    }
    
    if (is_null($person))
    {
      throw new LexisNexis_Soap_SessionException("No valid session for id: $sessionId");
    }
    
    $applicationAdminRoles = $this->_getAdminRolesForPerson($person->id);
    
    return array(
      'person' => $person,
      'adminRoles' => $applicationAdminRoles
    );
  }
  
  /**
   * Get the details of a person associated with a given session id
   *
   * @param string $personId
   * @return array 
   * <ul>
   * <li>Person object</li>
   * <li>Array of admin and roles</li>
   * </ul>
   * @throws LexisNexis_Soap_ProcessingException, Zend_Db_Exception
   */
  public function getPersonById($personId)
  {
    $this->_authenticate();
    $this->_logger->log(__METHOD__ . ' - personId = ' . $personId, Zend_Log::INFO);
    
    $person = $this->_getPersonById($personId);
    
    if (is_null($person))
    {
      throw new LexisNexis_Soap_ProcessingException("No person record found for id: $personId");
    }
    
    $applicationAdminRoles = $this->_getAdminRolesForPerson($person->id);
    
    return array(
      'person' => $person,
      'adminRoles' => $applicationAdminRoles
    );
  }
  
  /**
   * Add a new person record, if a person with the
   * same email exists, update that person record
   *
   * @param $person
   * @return stdClass
   * @throws LexisNexis_Soap_ProcessingException, Zend_Db_Exception
   */
  public function addPersonOnDuplicateUpdate($person)
  {
    $this->_authenticate();
    $this->_logger->log(__METHOD__ . ' - person = ' . print_r($person, true), Zend_Log::INFO);
    
    if (!$person->validate())
    {
      throw new LexisNexis_Soap_ParameterException('Invalid person data');
    }
    
    $personData = array(
      'family_name'     => $person->familyName,
      'given_name'      => $person->givenName,
      'middle_initials' => $person->middleInitials,
      'title'           => $person->title,
      'email'           => $person->email,
      'enable'              => $person->enable,
      'view_mentor_reports'   => $person->viewMentorReports
    );
    
    $personTable = new Zend_Db_Table('person');
    
    if (!empty($person->id))
    {
      // edit request - check person exists
      if ($storedPerson = $this->_getPersonById($person->id))
      {
        if (!empty($storedPerson->rosettaUserId))
        {
          // can only edit title and initials - everything else comes from Rosetta
          unset($personData['family_name']);
          unset($personData['given_name']);
          unset($personData['email']);
          unset($personData['password']);
        }
        
        if (!empty($person->password))
        {
          $personData['password'] = $person->password;
        }
        
        $where = $personTable->getAdapter()->quoteInto('id = ?', $person->id);
        $personTable->update($personData, $where);
      }
      else
      {
        throw new LexisNexis_Soap_ProcessingException('No matching person record');
      }
    }
    else
    {
      // new person record
      $personData['rosetta_user_id'] = empty($person->rosettaUserId) ? null : $person->rosettaUserId;
      $personData['password'] = empty($person->password) ? null : $person->password;
      $person->id = $personTable->insert($personData);
    }
    
    return $this->_getPersonById($person->id);
  }
  
  /**
   * Link an existing person account with a RAT account
   *
   * @param integer $personId
   * @param string $ratUserId
   * @param string $ratPassword
   * @return stdClass
   * @throws LexisNexis_Soap_ProcessingException, Zend_Db_Exception
   */
  public function ratLinkPersonAccount($personId, $ratUserId, $ratPassword)
  {
    // check person exists
    if ($storedPerson = $this->_getPersonById($personId))
    {
      if (!empty($storedPerson->rosettaUserId))
      {
        throw new LexisNexis_Soap_ProcessingException('Cannot update existing RAT account');
      }
    }
    else
    {
      throw new LexisNexis_Soap_ProcessingException('No matching person record');
    }
    
    if ($ratUserId && $ratPassword)
    {
      try
      {
        $wskClient = new Uis_Soap_WskClient();
        $userInfo = $wskClient->getUserInfo($ratUserId, $ratPassword);
      }
      catch (Exception $e)
      {
        throw new LexisNexis_Soap_ProcessingException('Error fetching user information from WSK');
      }
      
      $data = array(
        'rosetta_user_id' => $ratUserId,
        'password'        => null,
        'family_name'     => $userInfo->familyName,
        'given_name'      => $userInfo->givenName,
        'email'           => $userInfo->email
      );
      
      $personTable = new Zend_Db_Table('person');
      $where = $personTable->getAdapter()->quoteInto('id = ?', $personId);
      $personTable->update($data, $where);
    }
    else if ($ratUserId)
    {
      $data = array(
        'rosetta_user_id' => $ratUserId,
        'password'        => null
      );
      
      $personTable = new Zend_Db_Table('person');
      $where = $personTable->getAdapter()->quoteInto('id = ?', $personId);
      $personTable->update($data, $where);
    }
    
    return $this->_getPersonById($personId);
  }
  
  /**
   * Generate a password by concatenating words 
   * from a dictionary file with a random number
   *
   * @return string
   */
  public function generatePassword()
  {
    return PasswordGenerator::generate();
  }
  

  /**
   * test whether this user is an IP user
   */
  public function isIpUser($ipAddress)
  {

    $isIPuser = FALSE;
    try
    {
        $db = Zend_Registry::get('db');
        $statement = $db->prepare('SELECT ip_auth.password, person.email
                                     FROM ip_auth
                                LEFT JOIN person ON person_id = person.id  
                                    WHERE ip_auth.ip_address = :ipAddress
                                      AND ip_auth.enabled    > 0 ');
        $statement->bindValue('ipAddress',    $ipAddress);
        $statement->execute();
        $person = $statement->fetchAll();
        $statement->closeCursor();
    }
    catch (Exception $e)
    {
        return $isIPuser;
    }  

    if (isset($person[0]['email']) && strlen($person[0]['email']) > 1)
    {
        $isIPuser = TRUE;
    }

    return $isIPuser;
  }


  /**
   * Fetch a person record by person id
   *
   * @param int $personId
   * @return stdClass
   * @throws Zend_Db_Exception
   */
  private function _getPersonById($personId)
  { 
    $person = null;
    if (is_numeric($personId))
    {
      $personTable = new Zend_Db_Table('person');
      $result = $personTable->find($personId);
      
      if ($result->count() == 1)
      {
        $personData = $result->current();
        $person = new stdClass();
        $person->id = $personData->id;
        $person->rosettaUserId = $personData->rosetta_user_id;
        $person->familyName = $personData->family_name;
        $person->givenName = $personData->given_name;
        $person->middleInitials = $personData->middle_initials;
        $person->title = $personData->title;
        $person->email = $personData->email;
	$person->viewMentorReports = $personData->view_mentor_reports;
      }
    }
    
    return $person;
  }
  
  /**
   * Fetches a list of admin roles for a person for
   * the requesting application
   *
   * @param int $personId
   * @return array string[]
   * @throws Zend_Db_Exception
   */
  private function _getAdminRolesForPerson($personId)
  {
    $applicationAdminTable = new Zend_Db_Table('application_admin');
    $select = $applicationAdminTable->select()
      ->where('person_id = ?', $personId)
      ->where('application_id', $this->_applicationId);
    
    $applicationAdminRoles = array();
    if ($rows = $applicationAdminTable->fetchAll($select))
    {
      foreach ($rows as $row)
      {
        $applicationAdminRoles[] = $row['role'];
      }
    }
    
    return $applicationAdminRoles;
  }
  
  /**
   * Check if the request has passed authentication
   *
   * @return void
   * @throws LexisNexis_Soap_AuthException
   */
  private function _authenticate()
  {
    if(!$this->_authenticated){
      throw new LexisNexis_Soap_AuthException('Not authenticated');
    }
  }
}

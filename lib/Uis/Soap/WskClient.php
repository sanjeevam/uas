<?php

/**
 * SOAP client for calls to WSK
 *
 * @package Uis_Soap
 */

class Uis_Soap_WskClient
{
  /**
   * SOAP client for WSK authentication
   *
   * @var SoapClient
   */
  private $_authenticationClient;
  
  /**
   * SOAP client for WSK authorisation
   *
   * @var SoapClient
   */
  private $_authorisationClient;
  
  /**
   * Security token for WSK requests
   *
   * @string
   */
  private $_binarySecurityToken;
  
  /**
   * Date and time of security token expiry
   *
   * @var string
   */
  private $_binarySecurityTokenExpiration;
  
  /**
   * Logger
   *
   * @var Zend_Log
   */
  private $_logger = null;
  
  /**
   * Public constructor
   *
   * @return void
   * @throws SoapFault
   */
  public function __construct()
  {
    $this->_logger = Zend_Registry::get('appLog');
    
    $this->_authenticationClient = new SoapClient(
      dirname(__FILE__) . '/../../../wsk/wsdl/WSAPI-1_12.wsdl',
      array(
        'location' => 'https://www.lexisnexis.com/wsapi/v1/services/Authentication',
        'uri' => 'https://www.lexisnexis.com/wsapi/v1/services/Authentication',
        'trace' => 1
      )
    );
    
    $this->_authorisationClient = new SoapClient(
      dirname(__FILE__) . '/../../../wsk/wsdl/WSAPI-1_12.wsdl',
      array(
        'location' => 'https://www.lexisnexis.com/wsapi/v1/services/Authorization',
        'uri' => 'https://www.lexisnexis.com/wsapi/v1/services/Authorization',
        'trace' => 1
      )
    );
  }
  
  /**
   * Authenticate an end user
   *
   * @param string $userId
   * @param string $password
   * @return boolean
   * @throws SoapFault, Uis_Soap_Exception
   */
  public function authenticateEndUser($userId, $password)
  {
    $request = array(
      'wskCredentials' => array(
        'authId'    => 'jczir',
        'password'  => 'czirbesz',
        'namespace' => '00'
      ),
      'endUserCredentials' => array(
        'authId'    => 'dime0001',
        'password'  => 'mime0001',
      ),
      'product' => 'WSKGlobal'
    );
    
    try
    {
      $wskResponse = $this->_authenticationClient->authenticateEndUser($request);
    }
    catch (Exception $e)
    {
      $this->_logger->log(__METHOD__ . ' caught WSK SOAP fault: ' . $e->getMessage() . "\n" . $e->getTraceAsString(), Zend_Log::ERR);
      throw $e;
    }
    
    return true;
  }
  
  /**
   * Get user information
   *
   * @param string $userId
   * @param string $password
   * @return stdClass
   * @throws SoapFault, Uis_Soap_Exception
   */
  public function getUserInfo($userId, $password)
  {
    $this->authenticate();
    $request = array(
      'binarySecurityToken'       => $this->_binarySecurityToken,
      'userIdCredentials'         => array('userId' => $userId, 'password' => $password),
      'product'                   => array('namespace' => 'UK', 'productPackage' => 'Rosetta_UK_AuthOnly'),
      'returnSourcePackageIds'    => false,
      'returnUserAttributes'      => false,
      'returnIpRegistrationInfo'  => false,
      'returnLbuAccountId'        => true
    );

    try
    {
      $wskResponse = $this->_authorisationClient->getUserInfo($request);
    }
    catch (Exception $e)
    {
      $this->_logger->log(__METHOD__ . ' caught WSK SOAP fault: ' . $e->getMessage() . "\n" . $e->getTraceAsString(), Zend_Log::ERR);
      throw $e;
    }
    
    
    if (isset($wskResponse->lastName) && isset($wskResponse->firstName) && isset($wskResponse->emailAddress))
    {
      $userInfo = new stdClass;
      $userInfo->familyName = $wskResponse->lastName;
      $userInfo->givenName  = $wskResponse->firstName;
      $userInfo->email      = $wskResponse->emailAddress;
      return $userInfo;
    }
    else
    {
      $this->_logger->log(__METHOD__ . " Unexpected response from WSK::getUserInfo()\n" . print_r($wskResponse), Zend_Log::ERR);
      throw new Uis_Soap_Exception('Unexpected response from WSK::getUserInfo()');
    }
  }

  /**
   * Authenticate with WSK and set security token for subsequent requests
   *
   * @return void
   * @throws SoapFault, Uis_Soap_Exception
   */
  private function authenticate()
  {
    #TODO: check token expiration time before authenticating again
  
    $request = array(
      'authId'    => 'jczir',
      'password'  => 'czirbesz',
      'namespace' => '00'
    );

    try
    {
      $wskResponse = $this->_authenticationClient->authenticate($request);
    }
    catch (Exception $e)
    {
      $this->_logger->log(__METHOD__ . ' caught WSK SOAP fault: ' . $e->getMessage() . "\n" . $e->getTraceAsString(), Zend_Log::ERR);
      throw $e;
    }
    
    if (isset($wskResponse->binarySecurityToken) && isset($wskResponse->expiration))
    {
      $this->_binarySecurityToken = $wskResponse->binarySecurityToken;
      $this->_binarySecurityTokenExpiration = $wskResponse->expiration;
    }
    else
    {
      $this->_logger->log(__METHOD__ . " Unexpected response from WSK::authenticate()\n" . print_r($wskResponse), Zend_Log::ERR);
      throw new Uis_Soap_Exception('Unexpected response from WSK::authenticate()');
    }
  }
}

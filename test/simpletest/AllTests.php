<?php

require_once dirname(__FILE__) . '/TestHelper.php';

if (!defined('TEST_SUITE_MAIN_METHOD'))
{
  define('TEST_SUITE_MAIN_METHOD', 'AllTests::main');
}

class AllTests
{
  public static function main()
  {
    $suite = new TestSuite('UAS - all tests');
    $suite->addTestFile('Uas/TestSuite.php');
    $suite->addTestFile('Uas/Soap/TestSuite.php');
    $suite->run (new TextReporter);
  }
}

if (TEST_SUITE_MAIN_METHOD == 'AllTests::main')
{
    AllTests::main();
}

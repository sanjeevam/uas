<?php

/*
 * Get the config
 */
$platformConfigIniFile = '/var/sites/PZini/uas_test.ini';
$platformConfig = parse_ini_file($platformConfigIniFile, TRUE);

$zendLibDir = $platformConfig['lib']['zend_lib_dir'];
$lexisnexisLibDir = $platformConfig['lib']['lexis_nexis_lib_dir'];
$simpletestLibDir = '/opt/lib/simpletest_1.0.1/';
$modelLibDir = realpath(dirname(__FILE__) . '/../../lib');

/*
 * Add Zend and LexisNexis libraries to the include path
 */
set_include_path($zendLibDir . PATH_SEPARATOR . $lexisnexisLibDir . PATH_SEPARATOR . $modelLibDir . PATH_SEPARATOR . $simpletestLibDir . PATH_SEPARATOR . get_include_path());

/*
 * Zend Autoloader
 */
require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Uis_');

$config = new Zend_Config_Ini($platformConfigIniFile);
Zend_Registry::set('config', $config);

/**
 * Logging
 */
$writer = new Zend_Log_Writer_Stream($config->log->app);
$appLog = new Zend_Log($writer);
Zend_Registry::set('appLog', $appLog);
    
/*
 * Simpletest
 */
require_once 'unit_tester.php';
require_once 'reporter.php';


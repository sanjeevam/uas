<?php

class Uis_PersonTest extends UnitTestCase
{
  public function testValidate()
  {
    $person = new Uis_Person();

    // set basic values
    $person->password = 'pass';
    $person->givenName = 'test';
    $person->familyName = 'test';


    // email
    $person->email = 'email';
    $this->assertFalse($person->validate());

    $person->email = 'email@test.com';
    $this->assertTrue($person->validate());

    // name
    $person->givenName = '';
    $this->assertFalse($person->validate());
    $person->givenName = 'test';

    $person->familyName = '';
    $this->assertFalse($person->validate());
    $person->familyName = 'test';

    // password or rosetta id, not both
    $person->password = '';
    $this->assertFalse($person->validate());

    $person->rosettaUserId = 'test';
    $this->assertTrue($person->validate());

    $person->password = 'test';
    $this->assertFalse($person->validate());

    $person->rosettaUserId = '';
    $this->assertTrue($person->validate());
  }
}

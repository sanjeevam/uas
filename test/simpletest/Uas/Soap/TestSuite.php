<?php

require_once dirname(__FILE__) . '/../../TestHelper.php';

if (!defined('TEST_SUITE_MAIN_METHOD'))
{
  define('TEST_SUITE_MAIN_METHOD', 'Uas_Soap_TestSuite::main');
}

class Uas_Soap_TestSuite extends TestSuite
{
  public static function main()
  {
    $suite = new self;
    $suite->run(new TextReporter());
  }
  
  public function __construct()
  {
    $this->TestSuite('UAS SOAP tests');
    $this->addTestFile(dirname(__FILE__) . '/WskClientTest.php');
  }
}

if (TEST_SUITE_MAIN_METHOD == 'Uas_Soap_TestSuite::main')
{
    Uas_Soap_TestSuite::main();
}

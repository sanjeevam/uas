<?php

class Uis_Soap_WskClientTest extends UnitTestCase
{
  public function testAuthenticateEndUser()
  {
    $wskClient = new Uis_Soap_WskClient();
    
    try
    {
      $result = $wskClient->authenticateEndUser('dime0001', 'mime0001');
    }
    catch(Exception $e)
    {
      // Shouldn't get an exception - trigger an error for reporting
      $this->fail('Unexpected SOAP exception: ' . $e->getMessage());
    }
    
    $this->assertTrue($result);
  }

  public function testGetUserInfo()
  {
    $wskClient = new Uis_Soap_WskClient();
    
    try
    {
      $result = $wskClient->getUserInfo('dime0001', 'mime0001');
    }
    catch(Exception $e)
    {
      // Shouldn't get an exception - trigger an error for reporting
      $this->fail('Unexpected SOAP exception: ' . $e->getMessage());
    }
    
    $this->assertEqual($result->givenName, 'Sean');
    $this->assertEqual($result->familyName, 'Dwyer');
    $this->assertEqual($result->email, 'sean.dwyer@lexisnexis.co.uk');
    
    $exception = false;
    
    try
    {
      $result = $wskClient->getUserInfo('madeup', 'madeup');
    }
    catch (Exception $e)
    {
      $exception = true;
    
      // Should see a SOAP fault for this call
      $this->assertIsA($e, 'SoapFault');
      $this->assertTrue(strstr($e, 'Invalid Request'));
    }
    
    // Make sure we did in fact receive an exception
    $this->assertTrue($exception);
  }
}
